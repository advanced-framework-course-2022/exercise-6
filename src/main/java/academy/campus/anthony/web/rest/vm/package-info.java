/**
 * View Models used by Spring MVC REST controllers.
 */
package academy.campus.anthony.web.rest.vm;
