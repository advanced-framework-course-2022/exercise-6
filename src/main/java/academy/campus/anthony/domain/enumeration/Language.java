package academy.campus.anthony.domain.enumeration;

/**
 * The Language enumeration.
 */
public enum Language {
    FRENCH,
    ENGLISH,
    SPANISH,
}
