# Exercice 6

## Développement

Installer les dépendances:

```
npm install
```

Lancer l'application:
```
./mvnw
npm start
```

## Démarche d'ajout d'un attribut

**En prenant en compte les couches suivantes des deux applicatifs:**

- Côté back-end:
  - Mise à jour du modèle de la BDD
  - Domaine (classe métier)
  - DAO (repository)
  - Service
  - Controleur
  - DTO

- Côté front-end:
  - DTO
  - Modèle
  - Service
  - Vue (liste, détail, formulaire)

### Back-end

#### Migration BDD

Nous commençons par prévoir les migrations du modèle de la BDD en ajoutant:
```xml
<column name="business_id" type="varchar(255)" remarks="The business id attribute.">
    <constraints nullable="false" />
</column>
```

et 

```xml
<addUniqueConstraint
            columnNames="business_id"
            constraintName="business_id_unique"
            tableName="employee"
            />
```

pour avoir une unicité sur le champs

#### Domaine

Nous modifions ensuite l'entité métier `Employee` en ajoutant:
```java
@Column(name = "business_id", unique = true)
private String businessId;
```

`@Column` est une annotation fournie par Hibernate, permettant de faire la relation entre l'attribut `businessId` et la colonne `business_id` en base.


> **A noter**: Il ne faut pas oublier d'implémenter un mutateur et un accesseur ! Autrement les données ne pourront ni être lues ni modifiées.

```java
public String getBusinessId() {
    return this.businessId;
}

public void setBusinessId(String businessId) {
    this.businessId = businessId;
}

// Pour une écriture fluent.
public Employee businessId(String businessId) {
    this.setBusinessId(businessId);
    return this;
}
```

#### DAO

Nous n'avons pas besoin de mettre à jour la couche d'accès aux données étant donné que `EmployeeRepository` travaille déjà avec une entité de type `Employee`.

```java
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {}
```

#### Service

Nous n'avons pas de service métier à proprement parlé dans l'architecture proposée par JHipster.
Le repository est directement injecté dans le controlleur.

#### Controleur

Nous n'avons pas besoin de mettre à jour le controleur étant donné que `EmployeeResource` implémente déjà le CRUD sur l'entité de type `Employee`.

#### DTO

Concernant le DTO (Data Transfer Object), soit la réprésentation de la donnée exposée par l'API, JHipster a fait le choix dans son architecture de retourner directement les classes du domaine (pas de classe intermédiaire, ni de mapping).
Par conséquent, étant donné que l'on a déjà modifié la classe `Employee` nous avons déjà modifié le DTO.

### Front-end

#### Typage du DTO

Côté front-end, le DTO est typé par une interface appelée `IEmployee`.
Nous modifions ainsi l'interface de sorte à ajouter notre nouvel attribut: `businessId`.

```ts
export interface IEmployee {
  id?: number;
  businessId?: string | null; // Nouvelle ligne.
  firstName?: string | null;
  [...] // Code coupé par soucis de clareté.
}
```

### Conversion du DTO en classe métier

Étant donné que TypeScript est un langage transpilé vers du JavaScript, les interfaces disparaissent à la transpilation.
JHipster a alors fait le choix de convertir les DTO (sous format JSON) en classes.

Ainsi nous devons modifier la classe pour bien reporter le `businessId`:
```ts
export class Employee implements IEmployee {
  constructor(
    public id?: number,
    public businessId?: string | null, // Nouvelle ligne.
    public firstName?: string | null,
    [...] // Code coupé par soucis de clareté.
  ) {}
}
```

### Service

Le service `EmployeeService` est une classe fournissant des méthodes permettant de manipuler des employés (CRUD).
Cette classe fait abstraction de l'API pour les composants. Ainsi, les composants ne font à aucun moment des appels directs à l'API, mais plutôt des appels à l'`EmployeeService`.

Tout comme le service côté back-end, **aucune modification n'est requise** dans ce service, étant donné que les actions pour travailler avec des objets de type `Employee` sont déjà implémentées.

### Vues

Afin d'afficher le nouvel attribut, nous devons faire des modifications dans les composants d'affichage.

Ainsi, dans le template de `EmployeesComponent`, composant listant les employées, nous ajoutons:
```html
<td>{{ employee.businessId }}</td>
```

Et dans le template de `EmployeeDetailComponent`, composant décrivant un employé, nous ajoutons:
```html
<dt><span>Business ID</span></dt>
<dd>
  <span>{{ employee.businessId }}</span>
</dd>
```

**Aucune modification sur la logique des composants** n'est nécessaire, étant donné que la logique de récupération des données est déjà implémentées par la génération JHipster.

#### Formulaire

Concernant le formulaire contenu dans `EmployeeUpdateComponent` nous devons modifier la logique du composant.

Tout d'abord, le nouveau champs doit être ajouté dans le FormGroup (representation logique du formulaire):
```ts
editForm = this.fb.group({
  id: [],
  businessId: [], // Nouvelle ligne
  firstName: [],
  lastName: [],
  [...] // Code coupé par soucis de clareté.
});
```

Ensuite, nous devons ajouter le nouvel input côté template:
```html
<div class="row mb-3">
  <label class="form-label" for="field_businessId" ngbTooltip="The businessId attribute.">Business ID</label>
  <input type="text" class="form-control" name="businessId" id="field_businessId" data-cy="businessId" formControlName="businessId" />
</div>
```

> **Note:** Seul le formControlName est fonctionnellement important ici.

Pour finir l'ajout de notre nouvel attribut, nous devons ensuite modifier la logique de conversion du formulaire en entité `Employee`. Pour se faire, le code suivant a été modifié:

```ts
protected createFromForm(): IEmployee {
  return {
    ...new Employee(),
    id: this.editForm.get(['id'])!.value,
    businessId: this.editForm.get(['businessId'])!.value, // Nouvelle ligne
    firstName: this.editForm.get(['firstName'])!.value,
    [...] // Code coupé par soucis de clareté.
  };
}
```

Enfin, en cas d'édition d'un employé existant, nous devons être en capacité de reporter le `businessId` dans le formulaire:
```ts
protected updateForm(employee: IEmployee): void {
  this.editForm.patchValue({
    id: employee.id,
    businessId: employee.businessId,
    firstName: employee.firstName,
    [...] // Code coupé par soucis de clareté.
  });

  [...] // Code coupé par soucis de clareté.
}
```
